public class TestInheritance {
    public static void main(String[] args) {
        //This array type is abstract
        Account[] accArr = new Account[3];
        SavingsAccount acc1 = new SavingsAccount(2.0, "Jay");
        SavingsAccount sav = new SavingsAccount(4.0, "Jay");
        CurrentAccount cur = new CurrentAccount(6.0, "Jay");
        //Polymorphism allows you to place different types of accounts into account array
        accArr[0] = acc1;
        accArr[1] = sav;
        accArr[2] = cur; 
        
        for(int i = 0; i < accArr.length; i++ ){
            System.out.println("Before interest " + accArr[i].getBalance());
            accArr[i].addInterest(); 
            System.out.println("After interest " + accArr[i].getBalance());
        }
    }
}